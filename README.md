Silex Photographer CMS
========
This project is a silex CMS application for photographers.

## Features
- Back-office
- Pictures, Collections, Tags
- Update About page using Markdown
- Generated slugs for pictures
- Contact page
- Ajax pictures search
- Flash messages
- Pagination for pictures

## Usage
### Requirements
* [Node.js](https://nodejs.org/en/) installed
* [Composer](https://getcomposer.org/) installed

### Installation
- Use `composer install` on root folder
- Import Database using `db.sql`
- Update config values in `config/settings.yml`
- Update `public/.htaccess` file to setup your app entry point
- Set the `public` folder as Web server root

### Edit the project
If you wish to update project files, all assets sources are available in the `src` folder with a gulp configuration to update output assets.