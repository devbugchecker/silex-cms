<?php

namespace App\Controllers\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Collection;
use App\Models\Picture;

class CollectionController {

    public function index(Application $app, Request $request)
    {
        $collections = Collection::orderBy('created_at', 'desc')->get();

        return $app['twig']->render('pages/admin/collections.index.twig', [
            'page'  => 'collections',
            'collections' => $collections
        ]);
    }

    public function store(Application $app, Request $request)
    {
        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('admin.collections.store'));

        $formBuilder->add('title', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
            'label'    => 'Title',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
            'label'    => 'Description',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $find = Collection::where('title', $input['title'])->first();

            if($find) {
                $app['session']->getFlashBag()->add('error', "The collection already exists.");
            } else {
                $input['slug'] = $app['slugify']->slugify($input['title']);
                Collection::create($input);
                $app['session']->getFlashBag()->add('success', "You successfully created a new collection.");
                return $app->redirect($app['url_generator']->generate('admin.collections.index'));
            }
        }

        return $app['twig']->render('pages/admin/collections.store.twig', [
            'page' => 'collections',
            'collection_form' => $form->createView()
        ]);
    }

    public function edit(Application $app, Request $request)
    {
        $collection = Collection::find($request->get('id'));
        if(!$collection) {
            $app['session']->getFlashBag()->add('error', "The collection you requested doesn't exist.");
            return $app->redirect('/admin/collections');
        }
        
        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('admin.collections.edit', array('id' => $collection->id)));

        $formBuilder->add('title', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
            'label'    => 'Title',
            'trim'     => true,
            'required' => true,
            'data'     => $collection->title,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
            'label'    => 'Description',
            'trim'     => true,
            'required' => true,
            'data'     => $collection->description,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $find = Collection::where([
                ['title', $input['title']],
                ['id', '!=', $collection->id]
            ]);

            if($find->first()) {
                $app['session']->getFlashBag()->add('error', "The collection name already exists.");
            } else {
                $input['slug'] = $app['slugify']->slugify($input['title']);
                $collection->update($input);
                $app['session']->getFlashBag()->add('success', "You successfully updated a collection.");
                return $app->redirect($app['url_generator']->generate('admin.collections.index'));
            }
        }

        return $app['twig']->render('pages/admin/collections.edit.twig', [
            'page' => 'collections',
            'collection_form' => $form->createView()
        ]);
    }

    public function delete(Application $app, Request $request)
    {
        $collection = Collection::find($request->get('id'));
        if(!$collection) {
            $app['session']->getFlashBag()->add('error', "The collection you requested doesn't exist.");
            return $app->redirect($app['url_generator']->generate('admin.collections.index'));
        }

        $pictures = Picture::where('collection_id', $collection->id)->get();
        foreach($pictures as $picture) {
            $picture->collection()->dissociate();
            $picture->save();
        }

        $collection->delete();

        $app['session']->getFlashBag()->add('success', "You successfully deleted a collection.");
        return $app->redirect($app['url_generator']->generate('admin.collections.index'));
    }

}