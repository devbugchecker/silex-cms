<?php

namespace App\Controllers\Admin;

use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;

use Bootstrap\Application;
use App\Models\Setting;
use App\Helpers\Settings;

class HelloController {

    public function about(Application $app, Request $request)
    {
        $about = Setting::where('title', 'about')->first();

        if($request->getMethod() == 'POST') {
            Setting::updateOrCreate(
                ['title' => 'about'],
                ['value' => $request->get('input')]
            );

            $app['session']->getFlashBag()->add('success', "You successfully updated your bio.");
            return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
        }

        return $app['twig']->render('pages/admin/about.twig', [
            'page'  => 'about',
            'about' => $about
        ]);
    }

}