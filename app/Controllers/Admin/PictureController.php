<?php

namespace App\Controllers\Admin;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Picture;
use App\Models\Collection;
use App\Models\Tag;
use App\Helpers\Settings;

class PictureController {

    public function index(Application $app, Request $request)
    {
        $pictures = Picture::orderBy('created_at', 'desc')->get();

        return $app['twig']->render('pages/admin/pictures.index.twig', [
            'page'  => 'pictures',
            'pictures' => $pictures
        ]);
    }

    public function store(Application $app, Request $request)
    {
        $collections = Collection::all();
        $collections_array = [];

        foreach($collections as $collection) {
            $collections_array[$collection->title] = $collection->id;
        }
        
        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('admin.pictures.store'));

        $formBuilder->add('title', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
            'label'    => 'Title',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
            'label'    => 'Description',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('collection', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, [
            'label'    => 'Collection',
            'choices'  => $collections_array,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('media', \Symfony\Component\Form\Extension\Core\Type\FileType::class, [
            'label'    => 'Media',
            'required' => true,
            'constraints' => [
                new Assert\Image(), 
                new Assert\NotNull()
            ]
        ]);

        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $find = Picture::where('title', $input['title'])->first();

            if($find) {
                $app['session']->getFlashBag()->add('error', "The picture already exists.");
            } else {
                $input['slug'] = $app['slugify']->slugify($input['title']);

                $fileName = md5(uniqid()) . '.' . $input['media']->guessExtension();

                $input['media']->move(
                    Settings::$uploads,
                    $fileName
                );

                $picture = new Picture();
                $picture->title = $input['title'];
                $picture->description = $input['description'];
                $picture->slug = $input['slug'];
                $picture->media = $fileName;
                $picture->collection()->associate($input['collection']);
                $picture->save();

                $tags = [];

                if($request->get('taggles')) {
                    foreach($request->get('taggles') as $tag) {
                        $current = Tag::firstOrCreate(array('title' => $tag));
                        $tags[] = $current->id;
                    }
                }

                $picture->tags()->sync($tags);

                $app['session']->getFlashBag()->add('success', "You successfully created a new picture.");
                return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
            }
        }

        return $app['twig']->render('pages/admin/pictures.store.twig', [
            'page'  => 'pictures',
            'picture_form' => $form->createView()
        ]);
    }

    public function edit(Application $app, Request $request)
    {
        $picture = Picture::with('collection')->with('tags')->find($request->get('id'));

        if(!$picture) {
            $app['session']->getFlashBag()->add('error', "The picture you requested doesn't exist.");
            return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
        }

        $collections = Collection::all();
        $collections_array = [];

        foreach($collections as $collection) {
            $collections_array[$collection->title] = $collection->id;
        }
        
        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('admin.pictures.edit', array('id' => $picture->id)));

        $formBuilder->add('title', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
            'label'    => 'Title',
            'trim'     => true,
            'required' => true,
            'data'     => $picture->title,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
            'label'    => 'Description',
            'trim'     => true,
            'required' => true,
            'data'     => $picture->description,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('collection', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, [
            'label'    => 'Collection',
            'choices'  => $collections_array,
            'required' => true,
            'data'     => isset($picture->collection->id) ? $picture->collection->id : null,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('media', \Symfony\Component\Form\Extension\Core\Type\FileType::class, [
            'label'    => 'Media',
            'required' => false
        ]);

        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $find = Picture::where([
                ['title', $input['title']],
                ['id', '!=', $picture->id]
            ]);

            if($find->first()) {
                $app['session']->getFlashBag()->add('error', "The picture name already exists.");
            } else {
                $input['slug'] = $app['slugify']->slugify($input['title']);

                $picture->title = $input['title'];
                $picture->description = $input['description'];
                $picture->slug = $input['slug'];

                if($input['media']) {
                    $fileName = md5(uniqid()) . '.' . $input['media']->guessExtension();

                    $input['media']->move(
                        Settings::$uploads,
                        $fileName
                    );

                    $fs = new Filesystem();

                    try {
                        $fs->remove(Settings::$uploads . '/' . $picture->media);
                    } catch (IOExceptionInterface $e) {
                        echo "An error occurred while removing the old media.";
                    }

                    $picture->media = $fileName;
                }

                $tags = [];

                if($request->get('taggles')) {
                    foreach($request->get('taggles') as $tag) {
                        $current = Tag::firstOrCreate(array('title' => $tag));
                        $tags[] = $current->id;
                    }
                }

                $picture->tags()->sync($tags);
                $picture->collection()->associate($input['collection']);
                $picture->save();

                $app['session']->getFlashBag()->add('success', "You successfully updated a picture.");
                return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
            }
        }

        return $app['twig']->render('pages/admin/pictures.edit.twig', [
            'page'  => 'pictures',
            'picture_form' => $form->createView(),
            'tags' => json_encode($picture->tags),
            'media' => $picture->media
        ]);
    }

    public function delete(Application $app, Request $request)
    {
        $picture = Picture::with('collection')->find($request->get('id'));

        if(!$picture) {
            $app['session']->getFlashBag()->add('error', "The picture you requested doesn't exist.");
            return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
        }

        $fs = new Filesystem();

        try {
            $fs->remove(Settings::$uploads . '/' . $picture->media);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while removing the old media.";
        }

        $picture->delete();

        $app['session']->getFlashBag()->add('success', "You successfully deleted a picture.");
        return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
    }

} 