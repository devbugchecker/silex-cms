<?php

namespace App\Controllers\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Picture;
use App\Helpers\Settings;

class SearchController {

    public function pictures(Application $app, Request $request)
    {
        $query   = $request->get('query');
        $results = Picture::where('title', 'like', '%' . $query . '%')
                    ->orWhere('description', 'like', '%' . $query . '%')
                    ->get();

        $response = [
            'message' => 'ok',
            'url'     => Settings::getConfig()['url'],
            'results' => $results
        ];

        return $app->json($response);
    }

}