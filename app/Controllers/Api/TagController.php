<?php

namespace App\Controllers\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Tag;
use App\Helpers\Settings;

class TagController {

    public function index(Application $app, Request $request)
    {
       $tags = Tag::all();

        $response = [
            'message' => 'ok',
            'url'     => Settings::getConfig()['url'],
            'tags' => $tags
        ];

        return $app->json($response);
    }

}