<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Collection;

class CollectionController {

    public function index(Application $app, Request $request)
    {
        $pagesCurrent = (int) $request->get('page');
        if(!$pagesCurrent) $pagesCurrent = 0;
        
        $collections = Collection::orderBy('id', 'desc')->skip($pagesCurrent * 6)->take(6)->get();
        $pagesTotal = ceil(Collection::all()->count() / 6);

        if($pagesCurrent > ($pagesTotal - 1)) return $app->redirect($app['url_generator']->generate('collections'));

        return $app['twig']->render('pages/collections.index.twig', [
            'title' => 'Collections',
            'page' => 'collections',
            'collections' => $collections,
            'pagesTotal' => $pagesTotal,
            'pagesCurrent' => $pagesCurrent
        ]);
    }

    public function single(Application $app, Request $request)
    {
        $slug = $request->get('slug');

        $collection = Collection::with('pictures')->where('slug', $slug);

        if(!$collection->first()) {
            $app->abort(404, 'This picture does not exist.');
        }

        return $app['twig']->render('pages/collections.single.twig', [
            'title' => $collection->first()->title,
            'page' => 'collections',
            'collection' => $collection->first()
        ]);
    }

}