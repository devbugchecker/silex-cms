<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\Collection;
use App\Models\Picture;
use App\Models\Setting;
use App\Helpers\Settings;

class HomeController {

    public function index(Application $app, Request $request)
    {
        $pictures = Picture::orderBy('created_at', 'desc')->take(9)->get();
        $collections = Collection::orderBy('created_at', 'desc')->take(3)->get();

        return $app['twig']->render('pages/index.twig', [
            'title' => 'Home',
            'page' => 'index',
            'pictures' => $pictures,
            'collections' => $collections
        ]);
    }

    public function about(Application $app, Request $request)
    {
        $about = Setting::where('title', 'about')->first();

        return $app['twig']->render('pages/about.twig', [
            'title' => 'About me',
            'page' => 'about',
            'about' => $about
        ]);
    }

    public function contact(Application $app, Request $request)
    {
        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('contact'));

        $formBuilder->add('email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, [
            'label'    => 'Your email',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('subject', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
            'label'    => 'Your subject',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('message', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
            'label'    => 'Your message',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);

        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $message = new \Swift_Message();
            $message->setSubject($input['subject'] . ' (' . $input['email'] . ')');
            $message->setFrom(array($input['email']));
            $message->setTo(array(Settings::getConfig()['mail']['to']));
            $message->setBody($input['message']);

            $app['mailer']->send($message);

            $app['session']->getFlashBag()->add('success', "The email was successfully sent.");
            return $app->redirect($app['url_generator']->generate('contact'));
        }

        return $app['twig']->render('pages/contact.twig', [
            'page' => 'contact',
            'contact_form' => $form->createView()
        ]);
    }

}