<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;

use Bootstrap\Application;
use App\Models\Picture;

class PictureController {

    public function index(Application $app, Request $request)
    {
        $pagesCurrent = (int) $request->get('page');
        if(!$pagesCurrent) $pagesCurrent = 0;
        
        $pictures   = Picture::orderBy('id', 'desc')->skip($pagesCurrent * 9)->take(9)->get();
        $pagesTotal = ceil(Picture::all()->count() / 9);

        if($pagesCurrent > ($pagesTotal - 1)) return $app->redirect($app['url_generator']->generate('pictures'));

        return $app['twig']->render('pages/pictures.index.twig', [
            'title' => 'Pictures',
            'page' => 'pictures',
            'pictures' => $pictures,
            'pagesTotal' => $pagesTotal,
            'pagesCurrent' => $pagesCurrent
        ]);
    }

    public function single(Application $app, Request $request)
    {
        $slug = $request->get('slug');

        $picture = Picture::with('collection')->where('slug', $slug);
        
        $tags = [];
        foreach($picture->first()->tags as $tag) {
            $tags[] = $tag;
        }

        if(!$picture->first()) {
            $app->abort(404, 'This picture does not exist.');
        }

        return $app['twig']->render('pages/pictures.single.twig', [
            'title' => $picture->first()->title,
            'page' => 'pictures',
            'picture' => $picture->first(),
            'collection' => $picture->first()->collection ? $picture->first()->collection : null,
            'tags' => $tags
        ]);
    }

}