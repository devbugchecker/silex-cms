<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;

use Bootstrap\Application;
use App\Models\Tag;

class TagController {

    public function single(Application $app, Request $request)
    {

        $tag = Tag::with('pictures')->where('id', $request->get('id'));
        
        if(!$tag->first()) {
            $app->abort(404, 'This tag does not exist.');
        }

        return $app['twig']->render('pages/tags.single.twig', [
            'title' => $tag->first()->title,
            'page' => 'tags',
            'tag' => $tag->first()
        ]);
    }

}