<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Bootstrap\Application;
use App\Models\User;

class UserController {

    public function login(Application $app, Request $request)
    {

        $formBuilder = $app['form.factory']->createBuilder();
        $formBuilder->setMethod('post');
        $formBuilder->setAction($app['url_generator']->generate('auth.login'));

        $formBuilder->add('email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, [
            'label'    => 'Your email',
            'trim'     => true,
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);
        $formBuilder->add('password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class, [
            'label'    => 'Your password',
            'required' => true,
            'constraints' => [
                new Assert\NotBlank()
            ]
        ]);
        $formBuilder->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();

            $user = User::where('email', $input['email'])->first();

            if($user && password_verify($input['password'], $user->password)) {
                $app['session']->set('admin', $user);
                $app['session']->getFlashBag()->add('success', "You successfully logged in.");
                return $app->redirect($app['url_generator']->generate('admin.pictures.index'));
            } 
            
            else {
                $app['session']->getFlashBag()->add('error', "Username and password don't match.");
            }
        }

        return $app['twig']->render('pages/login.twig', [
            'title' => 'Log in',
            'contact_form' => $form->createView()
        ]);
    }

    public function logout(Application $app, Request $request)
    {
        $app['session']->remove('admin');
        return $app->redirect($app['url_generator']->generate('index'));
    }

}