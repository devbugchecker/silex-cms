<?php

namespace App\Helpers;

use Symfony\Component\Yaml\Yaml;

class Settings {

    public  static $config      = null;
    private static $environment = null;
    public  static $uploads     = __DIR__ . '/../../public/uploads';

    public static function getConfig() {
        if(self::$config === null) {
            $config_file  = Yaml::parse((file_get_contents(__DIR__ . '/../../config/settings.yml')));
            self::$config = $config_file;
            
            if(!self::$config) throw new Exception("Config file could not be loaded!");
        }

        return self::$config;
    }
    
}