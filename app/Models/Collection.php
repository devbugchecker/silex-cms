<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model {
    
    protected $table    = 'collections';
    protected $fillable = ['title', 'description', 'slug'];

    public function pictures()
    {
        return $this->hasMany('App\Models\Picture');
    }

}