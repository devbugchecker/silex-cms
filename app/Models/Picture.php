<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model {
    
    protected $table    = 'pictures';
    protected $fillable = ['title', 'description', 'media'];

    public function collection()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

}