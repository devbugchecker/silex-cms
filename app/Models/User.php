<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    protected $table    = 'users';
    protected $fillable = ['username', 'email', 'password'];
    protected $hidden   = ['password'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = password_hash($value, PASSWORD_BCRYPT, array('cost' => 9));
    }

}