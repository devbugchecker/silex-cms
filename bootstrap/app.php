<?php

namespace Bootstrap;

use Silex\Application as SilexApplication;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;
use Cocur\Slugify\Bridge\Silex2\SlugifyServiceProvider;
use Illuminate\Database\Capsule\Manager as Capsule;

use App\Helpers\Settings;

class Application extends SilexApplication {

    public function __construct()
    {
        parent::__construct();

        $this->register(new FormServiceProvider());
        $this->register(new SlugifyServiceProvider());
        $this->register(new TranslationServiceProvider());
        $this->register(new SessionServiceProvider());
        $this->register(new ServiceControllerServiceProvider());
        $this->register(new ValidatorServiceProvider());
        $this->register(new LocaleServiceProvider());
        $this->register(new SwiftmailerServiceProvider(), array(
            'swiftmailer.options' => array(
                'host'       => Settings::getConfig()['mail']['host'],
                'port'       => Settings::getConfig()['mail']['port'],
                'username'   => Settings::getConfig()['mail']['username'],
                'password'   => Settings::getConfig()['mail']['password'],
                'encryption' => Settings::getConfig()['mail']['encryption'],
                'auth_mode'  => Settings::getConfig()['mail']['auth_mode']
            )
        ));
        $this->register(new TwigServiceProvider(), array(
            'twig.path' => __DIR__ . '/../resources/views',
        ));
        $this->register(new HttpFragmentServiceProvider());

        $this['twig'] = $this->extend('twig', function ($twig, $app) {
            $title = new \Twig_Function('title', function ($title = null) {
                if($title) return $title . ' - ' . Settings::getConfig()['name'];
                else return Settings::getConfig()['name'];
            });

            $asset = new \Twig_Function('asset', function ($path) {
                return Settings::getConfig()['url'] . 'assets/' . $path;
            });

            $uploads = new \Twig_Function('uploads', function ($upload) {
                return Settings::getConfig()['url'] . 'uploads/' . $upload;
            });

            $excerpt = new \Twig_Function('excerpt', function ($content, $size = 300) {
                if(strlen($content) > 300) return substr($content, 0, $size) . '...';
                else return $content;
            });

            $errorProp = new \Twig_Function('errorProp', function ($value) {
                return ucfirst(preg_replace('/_/', " ", preg_replace('/\[|\]/', "", $value)));
            });

           $twig->addFilter(new \Twig_SimpleFilter('ids', function ($array) {
               $ids = [];

               foreach($array as $value) {
                   $ids[] = $value->id;
               }
               
                return $ids;
            }));

            $twig->addFilter(new \Twig_SimpleFilter('markdown', function ($content) {
                $Parsedown = new \Parsedown();

                return $Parsedown->text($content);
            }));

            $twig->addFunction($title);
            $twig->addFunction($asset);
            $twig->addFunction($errorProp);
            $twig->addFunction($excerpt);
            $twig->addFunction($uploads);
            $twig->addGlobal('title', Settings::getConfig()['name']);

            return $twig;
        });
    }

}

$app = new Application();

require(__DIR__ . '/middlewares.php');
require(__DIR__ . '/../routes/web.php');
require(__DIR__ . '/../routes/api.php');
require(__DIR__ . '/../config/capsule.php');

return $app;