<?php

use Bootstrap\Application;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

$adminMiddleware = function (Request $request, Application $app) {
    if(!$app['session']->get('admin')) return $app->redirect('/login');
};