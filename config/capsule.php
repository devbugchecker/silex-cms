<?php

use App\Helpers\Settings;
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => Settings::getConfig()['database']['adapter'],
    'host'      => Settings::getConfig()['database']['host'],
    'port'      => Settings::getConfig()['database']['port'],
    'database'  => Settings::getConfig()['database']['name'],
    'username'  => Settings::getConfig()['database']['username'],
    'password'  => Settings::getConfig()['database']['password'],
    'charset'   => Settings::getConfig()['database']['charset'],
    'collation' => Settings::getConfig()['database']['collation'],
]);

$capsule->bootEloquent();
$capsule->setAsGlobal();