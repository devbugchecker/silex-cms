<?php

use App\Helpers\Settings;

return [
    'paths' => [
        'migrations' => 'database/migrations',
        'seeds'      => 'database/seeds'
    ],
    'migration_base_class' => 'Database\Migration',
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database'        => 'dev',
        'dev' => [
            'adapter'     => Settings::getConfig()['database']['adapter'],
            'host'        => Settings::getConfig()['database']['host'],
            'name'        => Settings::getConfig()['database']['name'],
            'user'        => Settings::getConfig()['database']['username'],
            'pass'        => Settings::getConfig()['database']['password'],
            'port'        => Settings::getConfig()['database']['port'],
            'charset'     => Settings::getConfig()['database']['charset'],
            'collation'   => Settings::getConfig()['database']['collation'],
            'unix_socket' => Settings::getConfig()['database']['unix_socket'],
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null
        ]
    ]
];