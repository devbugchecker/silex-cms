<?php

namespace Database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;

use App\Helpers\Settings;

class Migration extends AbstractMigration {

    public $capsule;
    public $schema;

    public function init()
    {
        $this->capsule = new Capsule;
        $this->capsule->addConnection([
            'driver'      => 'mysql',
            'host'        => Settings::getConfig()['database']['host'],
            'port'        => Settings::getConfig()['database']['port'],
            'database'    => Settings::getConfig()['database']['name'],
            'username'    => Settings::getConfig()['database']['username'],
            'password'    => Settings::getConfig()['database']['password'],
            'charset'     => Settings::getConfig()['database']['charset'],
            'collation'   => Settings::getConfig()['database']['collation']
        ]);

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }
    
}