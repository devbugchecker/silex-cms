<?php

use Database\Migration;

class CreateUsers extends Migration {

    public function up()
    {
        $this->schema->create('users', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('users');
    }

}