<?php

use Database\Migration;

class CreatePictures extends Migration {
    public function up()
    {
        $this->schema->create('pictures', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longtext('description');
            $table->string('collection_id');
            $table->string('media');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('pictures');
    }
}
