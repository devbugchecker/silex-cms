<?php

use Database\Migration;

class CreateCollections extends Migration {
    public function up()
    {
        $this->schema->create('collections', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longtext('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('collections');
    }
}
