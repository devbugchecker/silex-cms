<?php

use Database\Migration;

class CreateSettings extends Migration {
    public function up()
    {
        $this->schema->create('settings', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->longtext('value');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('settings');
    }
}
