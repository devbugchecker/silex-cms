<?php

use Database\Migration;

class CreateTags extends Migration {
    public function up()
    {
        $this->schema->create('tags', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        $this->schema->create('picture_tag', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->integer('picture_id')->unsigned()->index();
            $table->integer('tag_id')->unsigned()->index();
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    public function down()
    {
        $this->schema->drop('tags');
        $this->schema->drop('pictures_tags');
    }
}
