<?php

use Phinx\Seed\AbstractSeed;

class UsersTabelSeeder extends AbstractSeed {

    public function run()
    {
        $this->table('users')->insert([
            'username' => 'root',
            'email' => 'root@gmail.com',
            'password' => password_hash('root', PASSWORD_BCRYPT, array('cost' => 9))
        ])->save();
    }

}

