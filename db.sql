-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mar 30 Mai 2017 à 10:25
-- Version du serveur :  5.5.42
-- Version de PHP :  7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cms`
--

-- --------------------------------------------------------

--
-- Structure de la table `collections`
--

CREATE TABLE `collections` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `collections`
--

INSERT INTO `collections` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Architecture', 'architecture', 'Sed blandit facilisis dui, ac malesuada sem aliquam eu. Aliquam ornare magna nulla, vel luctus mauris imperdiet et. Suspendisse blandit orci ut consequat interdum. Aenean malesuada eget est quis semper. Donec mollis pharetra arcu, et volutpat nibh luctus ac. Cras id ipsum at ex eleifend viverra eu et risus. Aliquam non nibh ornare, aliquam ipsum nec, euismod magna.', '2017-05-30 07:34:08', '2017-05-30 07:34:08'),
(2, 'People', 'people', 'Nulla facilisi. Vestibulum et ligula eleifend, luctus odio vitae, convallis purus. Phasellus tincidunt nulla sit amet felis interdum, in laoreet ex tristique. Curabitur quis lobortis nisl. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum eu interdum metus. Maecenas luctus eros in congue placerat. Nunc cursus pellentesque mi.', '2017-05-30 07:34:31', '2017-05-30 07:34:31'),
(3, 'Japan', 'japan', 'Ut lacinia, mauris at faucibus dapibus, sapien dolor tristique justo, vestibulum faucibus mauris odio molestie mauris. Donec sit amet lorem suscipit, tempor nulla id, scelerisque risus. Aliquam consequat metus ut sapien commodo, ac convallis metus venenatis. Vivamus elementum lectus quis vulputate imperdiet. Donec ac ante eget sem semper ullamcorper sed at enim.', '2017-05-30 07:34:53', '2017-05-30 07:34:53'),
(4, 'Abstract', 'abstract', 'Phasellus ornare magna ullamcorper, efficitur felis finibus, pretium turpis. Sed tortor urna, ullamcorper sed condimentum sit amet, cursus ut risus. Ut et magna a nulla maximus ultricies ut quis enim. Sed pellentesque tortor at lorem ornare, vel commodo neque molestie. In hendrerit leo nisi, aliquam euismod nibh porttitor et. Sed porta massa ut massa elementum volutpat.', '2017-05-30 07:35:16', '2017-05-30 07:35:16');

-- --------------------------------------------------------

--
-- Structure de la table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`) VALUES
(20170522183947, 'CreateUsers', '2017-05-30 05:31:19', '2017-05-30 05:31:19'),
(20170522201916, 'CreatePictures', '2017-05-30 05:31:19', '2017-05-30 05:31:19'),
(20170522202501, 'CreateCollections', '2017-05-30 05:31:19', '2017-05-30 05:31:19'),
(20170527192751, 'CreateSettings', '2017-05-30 05:31:19', '2017-05-30 05:31:19'),
(20170529105509, 'CreateTags', '2017-05-30 05:31:19', '2017-05-30 05:31:19');

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `collection_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `pictures`
--

INSERT INTO `pictures` (`id`, `title`, `slug`, `description`, `collection_id`, `media`, `created_at`, `updated_at`) VALUES
(1, 'Abstract white building', 'abstract-white-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', 'b76f1d8aeccb2974a5402a71a3516386.jpeg', '2017-05-30 07:42:08', '2017-05-30 07:42:08'),
(2, 'Small weird house', 'small-weird-house', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', 'ee341295c238f933187598e6b489a59e.jpeg', '2017-05-30 07:42:35', '2017-05-30 07:42:35'),
(3, 'Big dark building', 'big-dark-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', 'b0cfe8042576a90dfbd76b02996a23cd.jpeg', '2017-05-30 07:43:01', '2017-05-30 07:43:01'),
(4, 'Weird building', 'weird-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', '20c51065b91bba3b5089461788d07860.jpeg', '2017-05-30 07:43:33', '2017-05-30 07:43:33'),
(5, 'Building in forest', 'building-in-forest', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', '3bedd95873e5bb70e22220f55c4531ee.jpeg', '2017-05-30 07:44:04', '2017-05-30 07:44:04'),
(6, 'Huge white building', 'huge-white-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', 'bc33a2701728128df4c347ff00c98007.jpeg', '2017-05-30 07:44:29', '2017-05-30 07:44:29'),
(7, 'Dark passage', 'dark-passage', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '1', 'da7fb0adb21fe144dae63233b17bb619.jpeg', '2017-05-30 07:44:58', '2017-05-30 07:44:58'),
(8, 'Wood guirders', 'wood-guirders', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', 'd008a0131eca625683e6de80b8c4f316.jpeg', '2017-05-30 07:46:46', '2017-05-30 07:46:46'),
(9, 'Losanges Texture', 'losanges-texture', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', '973c349800ef7e589f03d6dba3133cc1.jpeg', '2017-05-30 07:47:13', '2017-05-30 07:47:13'),
(10, 'Dark material', 'dark-material', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', '4ad04f3cea4e760a0407c1c9fffc86a0.jpeg', '2017-05-30 07:47:35', '2017-05-30 07:47:35'),
(11, 'Round stones', 'round-stones', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', '50d89278796b9deecaf4cf53f4128c40.jpeg', '2017-05-30 07:48:18', '2017-05-30 07:48:18'),
(12, 'White losanges texture', 'white-losanges-texture', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', '81dd48181ebb420cee996b5b281e6527.jpeg', '2017-05-30 07:48:51', '2017-05-30 07:48:51'),
(13, 'Strange tree in building', 'strange-tree-in-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '4', 'b0765692597fc5397953b289fb61b682.jpeg', '2017-05-30 07:49:18', '2017-05-30 07:49:18'),
(14, 'Little street', 'little-street', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', '132d8b33e944b7f57cfb417a8a19ba81.jpeg', '2017-05-30 07:50:20', '2017-05-30 07:50:20'),
(15, 'Orange way', 'orange-way', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', '3ee12c7b811943827812d8fadfe4a3c7.jpeg', '2017-05-30 07:50:43', '2017-05-30 07:50:43'),
(16, 'White boat', 'white-boat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', '6c8cf3c8cc2e54e937f405ef7a1ee878.jpeg', '2017-05-30 07:51:03', '2017-05-30 07:51:03'),
(17, 'Inside dark building', 'inside-dark-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', '512ac43430e886a8b0dcc93a848cd55d.jpeg', '2017-05-30 07:51:26', '2017-05-30 07:51:26'),
(18, 'Street with umbrellas', 'street-with-umbrellas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', 'c498370d9bd3f1bb3152e65985ab58a8.jpeg', '2017-05-30 07:52:26', '2017-05-30 07:52:26'),
(19, 'Inside plane', 'inside-plane', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '3', 'd1ed50a7affbccbfd5400bb9197ff6c2.jpeg', '2017-05-30 07:52:55', '2017-05-30 07:52:55'),
(20, 'Behind a rock', 'behind-a-rock', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'a23de044962b9212538b6ba0fd3af35d.jpeg', '2017-05-30 07:53:23', '2017-05-30 07:53:23'),
(21, 'Myself in a mirror', 'myself-in-a-mirror', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'b924cc799be2f83158976151df6e6fb1.jpeg', '2017-05-30 07:53:51', '2017-05-30 07:53:51'),
(22, 'Girlfriend w/Coffee', 'girlfriend-w-coffee', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'db0a80914ba4fd135a0e6b820eb429a0.jpeg', '2017-05-30 07:54:13', '2017-05-30 07:54:13'),
(23, 'Walking in the street', 'walking-in-the-street', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'fb15b17ad58f546ae8d82e9df25c11a5.jpeg', '2017-05-30 07:54:40', '2017-05-30 07:54:40'),
(24, 'Cool coat', 'cool-coat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'd91b237bcfa36b7e42cd7cd170abd2cc.jpeg', '2017-05-30 07:55:07', '2017-05-30 07:55:07'),
(25, 'Sunny beard', 'sunny-beard', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'e4cfa4922d1f6f35a5e6062acd012efe.jpeg', '2017-05-30 07:55:31', '2017-05-30 07:55:31'),
(26, 'Girl inside building', 'girl-inside-building', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'b1f64067f2762f95efa132b4dc163e3d.jpeg', '2017-05-30 07:55:50', '2017-05-30 07:55:50'),
(27, 'Cool outfit', 'cool-outfit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna rhoncus, facilisis eros eget, tristique diam. Praesent quis nisi sed turpis rhoncus vulputate nec nec massa. Integer varius libero sapien, id aliquam ligula venenatis quis. Morbi et nisl in sem placerat ullamcorper. Nulla ut erat tempus justo tincidunt fringilla ac vel nisi. Nam sed ipsum sit amet massa eleifend vehicula et nec metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor nibh felis, sed commodo est bibendum sed. Maecenas maximus mi eget augue bibendum venenatis. Fusce venenatis, sapien vitae interdum fringilla, orci tortor faucibus quam, eget tristique nulla dui at nisl. Fusce fringilla auctor rutrum. Nunc egestas lacus metus, vel euismod turpis lobortis id.', '2', 'd563f9763b122eaebf57c5568fec4ab1.jpeg', '2017-05-30 07:56:10', '2017-05-30 07:56:10');

-- --------------------------------------------------------

--
-- Structure de la table `picture_tag`
--

CREATE TABLE `picture_tag` (
  `id` int(10) unsigned NOT NULL,
  `picture_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `picture_tag`
--

INSERT INTO `picture_tag` (`id`, `picture_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(5, 3, 2),
(6, 3, 4),
(7, 4, 2),
(8, 5, 2),
(9, 5, 5),
(10, 6, 2),
(11, 6, 3),
(12, 7, 4),
(13, 7, 6),
(14, 8, 7),
(15, 8, 3),
(16, 9, 3),
(17, 9, 8),
(18, 10, 4),
(19, 11, 9),
(20, 11, 10),
(21, 11, 3),
(22, 12, 8),
(23, 12, 3),
(24, 13, 7),
(25, 13, 2),
(26, 14, 11),
(27, 14, 4),
(28, 15, 12),
(29, 15, 11),
(30, 16, 3),
(31, 16, 13),
(32, 17, 4),
(33, 17, 2),
(34, 18, 11),
(35, 18, 4),
(36, 19, 14),
(37, 20, 15),
(38, 20, 16),
(39, 21, 17),
(40, 22, 16),
(41, 23, 17),
(42, 24, 16),
(43, 25, 17),
(44, 26, 16),
(45, 27, 17);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`, `created_at`, `updated_at`) VALUES
(1, 'about', '## Tobias van Schneider is a German multi-disciplinary maker of useful, curious and beautiful things. (he just doesn''t like the word entrepreneur) Originally born in Germany, and raised in Austria, he now lives and works in New York City.\r\n\r\nTobias’ journey is a little different than most. He started his career by dropping out of high school at 15, and then made grand plans of becoming a software engineer. After realising he, quite frankly, sucked at it, Tobias began teaching himself design at the age of 16. Fast forward a few years and through a whole lot of ass-busting, Tobias is now a designer, founder, speaker, mentor and maker.\r\n\r\nAfter opening his own design studio, Tobias had the honor to develop products and services in a wide range of categories for companies such as Red Bull, Google, BMW, Wacom, Sony, Fantasy Interactive, Stinkdigital, Toyota, Ralph Lauren, Bwin, to name a few.\r\n\r\nAs part of his quest for overachievement, Tobias has been honored with the net magazine Designer of the Year & Awwwards Art Director of the Year award. He also serves as a member on the Board of Directors at AIGA NYC and is supporter, advisor and mentor to a variety of programs around the world, including Cannes Lions, HyperIsland, Art Directors Club & the FWA.\r\n\r\nFor the past two and a half years, Tobias has helped building new products as Art Director & Lead Product Designer at Spotify in New York. During this time, Spotify grew from 15 million users to 70+ million, becoming the leading music streaming service in the world.\r\n\r\nTobias’ work has been published in highly acclaimed press such as FastCompany, Inc Magazine, net Magazine, Computer Arts, Wired, FirstRound Capital, BusinessInsider, Washington Post, TheNextWeb.\r\n\r\nIn his spare time Tobias is the co-founder of Semplice, a portfolio system and community used by thousands of the world’s leading designers who represent brands such as HUGE, BBDO, Tumblr, Disney, Apple, Firstborn, Unit9, R/GA and Razorfish. Since the launch, the Semplice has quickly grown into one of the leading portfolio systems for creatives.\r\n\r\nIn addition to his mentoring work, Tobias is advisor and creative director at memomi. He’s played a crucial role in creating the brand and overseeing the user experience to help establish the world’s first digital mirror software platform called Memory Mirror. Memomi aims to revolutionize the interactive shopping experience and recently launched with selected partners such as Intel, IBM, Sony and Panasonic.', NULL, '2017-05-30 07:37:19');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'abstract', '2017-05-30 07:42:08', '2017-05-30 07:42:08'),
(2, 'building', '2017-05-30 07:42:08', '2017-05-30 07:42:08'),
(3, 'white', '2017-05-30 07:42:35', '2017-05-30 07:42:35'),
(4, 'dark', '2017-05-30 07:43:01', '2017-05-30 07:43:01'),
(5, 'green', '2017-05-30 07:44:04', '2017-05-30 07:44:04'),
(6, 'passage', '2017-05-30 07:44:58', '2017-05-30 07:44:58'),
(7, 'wood', '2017-05-30 07:46:46', '2017-05-30 07:46:46'),
(8, 'texture', '2017-05-30 07:47:13', '2017-05-30 07:47:13'),
(9, 'stone', '2017-05-30 07:48:18', '2017-05-30 07:48:18'),
(10, 'rock', '2017-05-30 07:48:18', '2017-05-30 07:48:18'),
(11, 'street', '2017-05-30 07:50:20', '2017-05-30 07:50:20'),
(12, 'orange', '2017-05-30 07:50:43', '2017-05-30 07:50:43'),
(13, 'boat', '2017-05-30 07:51:03', '2017-05-30 07:51:03'),
(14, 'plane', '2017-05-30 07:52:55', '2017-05-30 07:52:55'),
(15, 'people', '2017-05-30 07:53:23', '2017-05-30 07:53:23'),
(16, 'girl', '2017-05-30 07:53:23', '2017-05-30 07:53:23'),
(17, 'vanschneider', '2017-05-30 07:53:51', '2017-05-30 07:53:51');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'root', 'root@gmail.com', '$2y$09$1x7iKLeaczZ3dcx7/s3HIekvp2UUsz/6J8uxKoSKhfkWqVa/3M0TO', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `picture_tag`
--
ALTER TABLE `picture_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `picture_tag_picture_id_index` (`picture_id`),
  ADD KEY `picture_tag_tag_id_index` (`tag_id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `picture_tag`
--
ALTER TABLE `picture_tag`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `picture_tag`
--
ALTER TABLE `picture_tag`
  ADD CONSTRAINT `picture_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `picture_tag_picture_id_foreign` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
