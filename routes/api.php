<?php

$app->get('/api/search/pictures/{query}', 'App\Controllers\Api\SearchController::pictures')
    ->bind('api.search.pictures');

$app->get('/api/tags', 'App\Controllers\Api\TagController::index')
    ->bind('api.tags');