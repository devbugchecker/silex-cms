<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Bootstrap\Application;

$app->get('/', 'App\Controllers\HomeController::index')
    ->bind('index');

$app->get('/pictures/{page}', 'App\Controllers\PictureController::index')
    ->assert('page', '[0-9]*')
    ->value('page', '')
    ->bind('pictures');

$app->get('/pictures/{slug}', 'App\Controllers\PictureController::single')
    ->assert('slug', '[a-z0-9-]+')
    ->bind('pictures.single');

$app->get('/collections/{page}', 'App\Controllers\CollectionController::index')
    ->assert('page', '[0-9]*')
    ->value('page', '')
    ->bind('collections');

$app->get('/collections/{slug}', 'App\Controllers\CollectionController::single')
    ->assert('slug', '[a-z0-9-]+')
    ->bind('collections.single');

$app->get('/tags/{id}', 'App\Controllers\TagController::single')
    ->assert('id', '[0-9]+')
    ->bind('tags.single');

$app->get('/about', 'App\Controllers\HomeController::about')
    ->bind('about');

$app->match('/contact', 'App\Controllers\HomeController::contact')
    ->method('GET|POST')
    ->bind('contact');


$app->match('/login', 'App\Controllers\UserController::login')
    ->method('GET|POST')
    ->bind('auth.login');

$app->get('/logout', 'App\Controllers\UserController::logout')
    ->bind('auth.logout');


$app->get('/admin', 'App\Controllers\Admin\PictureController::index')
    ->before($adminMiddleware)
    ->bind('admin.pictures.index');

$app->match('/admin/pictures/add', 'App\Controllers\Admin\PictureController::store')
    ->method('GET|POST')
    ->before($adminMiddleware)
    ->bind('admin.pictures.store');

$app->match('/admin/pictures/{id}/edit', 'App\Controllers\Admin\PictureController::edit')
    ->method('GET|POST')
    ->assert('id', '[0-9]+')
    ->before($adminMiddleware)
    ->bind('admin.pictures.edit');

$app->get('/admin/pictures/{id}/delete', 'App\Controllers\Admin\PictureController::delete')
    ->before($adminMiddleware)
    ->assert('id', '[0-9]+')
    ->bind('admin.pictures.delete');

$app->get('/admin/collections', 'App\Controllers\Admin\CollectionController::index')
    ->before($adminMiddleware)
    ->bind('admin.collections.index');


$app->match('/admin/collections/add', 'App\Controllers\Admin\CollectionController::store')
    ->method('GET|POST')
    ->before($adminMiddleware)
    ->bind('admin.collections.store');

$app->match('/admin/collections/{id}/edit', 'App\Controllers\Admin\CollectionController::edit')
    ->method('GET|POST')
    ->assert('id', '[0-9]+')
    ->before($adminMiddleware)
    ->bind('admin.collections.edit');

$app->get('/admin/collections/{id}/delete', 'App\Controllers\Admin\CollectionController::delete')
    ->before($adminMiddleware)
    ->assert('id', '[0-9]+')
    ->bind('admin.collections.delete');

$app->match('/admin/about', 'App\Controllers\Admin\HelloController::about')
    ->method('GET|POST')
    ->before($adminMiddleware)
    ->bind('admin.about');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    switch ($code) {
        case 404:
            return $app['twig']->render('pages/404.twig', [
                'title' => 'Page not found',
                'page' => 'error'
            ], 404);
            break;
        default:
            return $app['twig']->render('pages/error.twig', [
                'title' => 'Error occured',
                'page' => 'error'
            ]);
    }
});
