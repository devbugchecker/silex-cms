let source = JSON.parse(document.querySelector('.form-tags').getAttribute('data-source'))
let source_tags = []
source.forEach((tag) => {
    source_tags.push(tag.title)
})

let taggle = new Taggle('form-tags', {
    tags: source_tags,
    duplicateTagClass: 'bounce'
})

let container = taggle.getContainer()
let input     = taggle.getInput()

let tags = new Promise((resolve, reject) => {
    var xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let res = JSON.parse(this.responseText)
            let tags = []

            res.tags.forEach((tag) => {
                tags.push(tag.title)
            })

            resolve(tags)
        }
    }
    xhttp.open('GET', '/api/tags', true)
    xhttp.send()
})

tags.then((res) => {
    $(input).autocomplete({
        source: res,
        appendTo: container,
        position: { at: "left bottom", of: container },
        select: function(event, data) {
            event.preventDefault();
            if (event.which === 1) {
                taggle.add(data.item.value);
            }
        }
    })
})