let header_search = document.querySelector('.header__nav__item--search')

if(header_search) {
    header_search.addEventListener('click', (e) => {
        e.preventDefault()
        let search = document.querySelector('.search')
        search.classList.add('search--active')
        let body   = document.querySelector('body')
        body.classList.add('body--blocked')
    })
}

let search_close = document.querySelector('.search__close')

if(search_close) {
    search_close.addEventListener('click', (e) => {
        e.preventDefault()
        let search = document.querySelector('.search')
        search.classList.remove('search--active')
        let body   = document.querySelector('body')
        body.classList.remove('body--blocked')
    })
}

let search_input = document.querySelector('.search__input__input')

if(search_input) {
    search_input.addEventListener('keyup', () => {
        let query = search_input.value
        let $results = document.querySelector('.search__results')

        $results.innerHTML = ''

        if(query != '') {
            let xhttp = new XMLHttpRequest()
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    let res = JSON.parse(this.responseText)

                    if(res.results.length == 0) {
                        $results.innerHTML = `<p>Sorry but nothing match your request :(</p>`
                    }

                    else if(res.results.length > 0) {
                        let $ul = document.createElement('ul')

                        res.results.forEach((result) => {
                            let $li = document.createElement('li')
                            $li.classList.add('search__item')

                            $li.innerHTML = `
                                <a href="${res.url}pictures/${result.slug}" class="search__link">
                                    <div class="search__picture" style="background-image: url('${res.url}uploads/${result.media}')"></div>
                                    <h3 class="search__title">${result.title}</h3>
                                    <p class="search__date">${moment(result.created_at).fromNow()}</p>
                                </a>
                            `

                            $ul.appendChild($li)
                        })

                        $results.appendChild($ul)
                    }

                    else {
                        $results.innerHTML = ''
                    }
                }
            }
            
            xhttp.open('GET', `/api/search/pictures/${query}`, true)
            xhttp.send()
        }
    })
}