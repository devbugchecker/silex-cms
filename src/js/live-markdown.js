let Converter = new showdown.Converter()

let input  = document.querySelector('.live-markdown__input textarea') 
let output = document.querySelector('.live-markdown__output__content')

let content = input.value
output.innerHTML = Converter.makeHtml(content)
    
input.addEventListener('keyup', (e) => {
    let content = input.value
    output.innerHTML = Converter.makeHtml(content)
})